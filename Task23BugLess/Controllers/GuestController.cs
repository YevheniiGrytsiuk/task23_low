﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Models;

namespace Task23.Controllers
{
    public class GuestController : Controller
    {
        private static List<FeedbackModel> _feedbacks;
        static GuestController() 
        {
            _feedbacks = new List<FeedbackModel>()
            {
                new FeedbackModel(0, "megakiller_2009", DateTime.Now, "Hello"),
                new FeedbackModel(0, "1223", DateTime.Now, "Бобало геній"),
                new FeedbackModel (0, "naruto@ss@in", DateTime.Now, "люблю доту"),
                new FeedbackModel(0, "test", DateTime.Now, "завтра в школу"),
                new FeedbackModel(0, "megakiller_2009", DateTime.Now, "Hello"),

                new FeedbackModel(1, "Nazar", DateTime.Now, "Green bottle blue"),
                new FeedbackModel (1, "Math", DateTime.Now, "люблю math"),
                new FeedbackModel(1, "testik111", DateTime.Now, "рівне макдональдс"),
                new FeedbackModel(1, "megakiller_2009", DateTime.Now, "Hello"),
                
                new FeedbackModel(2, "1223", DateTime.Now, "Бобало геній"),
                new FeedbackModel (2, "naruto@ss@in", DateTime.Now, "люблю доту"),
                
                new FeedbackModel (3, "naruto@ss@in", DateTime.Now, "люблю доту"),

                new FeedbackModel(4, "megakiller_2009", DateTime.Now, "Hello"),
                new FeedbackModel(4, "1223", DateTime.Now, "Бобало геній")
                
                //new FeedbackModel (5, "naruto@ss@in", DateTime.Now, "люблю доту"),
                //new FeedbackModel(5, "test", DateTime.Now, "завтра в школу"),
                //new FeedbackModel(5, "megakiller_2009", DateTime.Now, "Hello"),
                //new FeedbackModel(5, "1223", DateTime.Now, "Бобало геній"),
                //new FeedbackModel (6, "naruto@ss@in", DateTime.Now, "люблю доту"),
                //new FeedbackModel(6, "test", DateTime.Now, "завтра в школу"),
            };
        }
        public ActionResult Index()
        {
            return View(_feedbacks);
        }
        [HttpGet]
        public ActionResult Feedback(int id)
        {
            var blogFeedback = _feedbacks.Where(feedback => feedback.BlogId == id);
            return View(blogFeedback);
        }
    }
}