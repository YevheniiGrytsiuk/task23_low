﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Models;

namespace Task23.Controllers
{
    public class HomeController : Controller
    {
        private static Collection<BlogModel> _blogs;
        private static int AutoIncrement { get { return _autoIncrement++; } }
        private static int _autoIncrement;

        static HomeController()
        {
            _blogs = new Collection<BlogModel>()
            {
                new BlogModel()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Parse("4/22/2021 12:38:31 AM")
                },
                 new BlogModel()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Parse("4/22/2021 12:38:31 AM")
                },
                  new BlogModel()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Parse("4/22/2021 12:38:31 AM")
                },
                   new BlogModel()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Parse("4/22/2021 12:38:31 AM")
                }, new BlogModel()
                {
                    Id = AutoIncrement,
                    Header =  "Getting started",
                    Content = "ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that enables a clean separation of concerns and gives you full control over markup for enjoyable, agile development.",
                    AuthorName = "James",
                    CreationTime = DateTime.Parse("4/22/2021 12:38:31 AM")
                }
            };
        }
        public ActionResult Index()
        {
            return View(_blogs);
        }

    }
}