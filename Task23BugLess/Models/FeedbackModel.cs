﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23BugLess.Models
{
    public class FeedbackModel
    {
        public string AuthorName { get; set; }
        public DateTime CreationTime { get; set; }
        public string Content { get; set; }
        public int BlogId { get; set; }

        public FeedbackModel(int blogId, string authorName, DateTime time, string content)
        {
            BlogId = blogId;
            AuthorName = authorName;
            CreationTime = time;
            Content = content;
        }
    }
}