﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23BugLess.Models
{
    public class BlogModel
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Content{ get; set; }
        public string AuthorName { get; set; }
        public DateTime CreationTime { get; set; }
    }
}